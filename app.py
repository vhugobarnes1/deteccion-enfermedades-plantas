import base64
import json
from io import BytesIO

import numpy as np
import requests
from flask import Flask, request, jsonify
from flask_cors import CORS
import tensorflow as tf
from tensorflow.keras.preprocessing import image
import os

app = Flask(__name__)

@app.route('/cassava', methods=['POST'])
def predict():
    if request.method == 'POST':

        # Ruta del modelo
        filepath = os.path.dirname(os.path.abspath(__file__))
        model_path = filepath + '/models/cassavacnn'

        # Recogemos de la request la imagen
        image_to_predict = request.files['image']

        # Cargamos la imagen
        img = image.load_img(image_to_predict.filename, target_size=(1024,1024))
        # Convertimos la imagen a un array de numpy
        numpy_img = np.array(img)
        # Expandimos dimensiones del array
        numpy_img = np.expand_dims(numpy_img, axis=0)

        # Carga y crea el modelo exacto, incluyendo los pesos y el optimizador
        model = tf.keras.models.load_model(model_path)

        # Predice la clase de la imagen de entrada al modelo cargado
        predicted = model.predict(numpy_img)
        print(predicted) # Debug

        # CBB
        # CBSD
        # CGM
        # CMD
        # HEALTHY

        condicion = 'Null'

        if(([[1., 0., 0., 0., 0.]] == predicted).all()):
            condicion = 'Cassava Bacterial Blight'
        elif(([[0., 1., 0., 0., 0.]] == predicted).all()):
            condicion = 'Cassava Brown Streak Disease'
        elif(([[0., 0., 1., 0., 0.]] == predicted).all()):
            condicion = 'Cassava Green Mottle'
        elif(([[0., 0., 0., 1., 0.]] == predicted).all()):
            condicion = 'Cassava Mosaic Disease'
        elif(([[0., 0., 0., 0., 1.]] == predicted).all()):
            condicion = 'HEALTHY'
        
        return jsonify(condicion);


if __name__ == '__main__':
    
    app.run(port=3000, debug=True)